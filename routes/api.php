<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/signup', 'Usercontroller@store');
Route::post('/signin', 'Authcontroller@store');
Route::middleware('auth:api')->post('/signout', [AuthController::class, 'destroy']);
Route::middleware('auth:api')->get('/posts', 'PostController@index');
Route::middleware('auth:api')->group(function () {
    //posts
    Route::get('/posts', 'PostController@index');
    Route::get('/posts/{post}', 'PostController@show');
    Route::post('/posts/store', 'PostController@store');
    Route::get('/posts/{post}/edit', 'PostController@edit');
    Route::put('/posts/{post}/update', 'PostController@update');

    //posts likes
    Route::post('/post/{post}/like', 'PostLikeController@store');

    //comments
    Route::post('/posts/{post}/comment', 'CommentController@store');

    //follow
    Route::post('/users/{user}/follow', 'FollowController@store');
});
