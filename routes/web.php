<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;



Auth::routes();

Route::get('/', function(Request $request) {
	dd($request->headers);
});
// Route::get('/', 'HomeController@index')->name('home');

//posts
Route::get('/posts', 'PostController@index');
Route::get('/posts/create', 'PostController@create');
Route::post('/posts/store', 'PostController@store');
Route::post('/post/{post}/delete', 'PostController@destroy');
Route::get('/posts/{post}/edit', 'PostController@edit');
Route::patch('/posts/{post}/update', 'PostController@update');

Route::get('/posts/{post}', 'PostController@show');


//comments
Route::post('/posts/{post}/comment', 'CommentController@store');
Route::get('/comments/{comment}', 'CommentController@reply');
Route::post('/comments/{comment}/reply', 'CommentController@replystore');


//comment likes
Route::post('/comments/{comment}/like', 'CommentLikeController@store');
Route::delete('/comments/{comment}/dislike', 'CommentLikeController@dislike');



//posts likes
Route::post('/post/{post}/like', 'PostLikeController@store');
Route::post('/post/{post}/dislike', 'PostLikeController@destroy');


//users follow
Route::post('/users/{user}/follow', 'FollowController@store');

//ajax comments
Route::get('/posts/{post}/commentsload', 'CommentController@index');

//users
Route::get('/users/{user}', 'UserController@show');

//profile
Route::get('/profile', 'ProfileController@index');

//post images 
Route::get('/postimage/{post}', 'PostController@showImage');

//black list routes

Route::get('/blacklist', 'BlacklistController@index');
Route::get('/blacklist/{post}/create', 'BlacklistController@create');
Route::post('/blacklist/{post}/store', 'BlacklistController@store');


//api test for first time 

Route::get("/posts", 'PostController@apiIndex');
// Route::get("/posts/{post}")
// Route::post('/signup', 'ApiLoginRegisterController@register');
// Route::post('/signup' , function() {
// 	// return response()->json(['message' => 'hi']);
// 	return response()->json(request());

// });
Route::get('/posts/{post}/comments', 'CommentController@index');
Route::get('/comments/{comment}', 'CommentController@show');


