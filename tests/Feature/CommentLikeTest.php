<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommentLikeTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // public function testExample()
    // {
    //     $response = $this->get('/');

    //     $response->assertStatus(200);
    // }

    public function test_authenticated_user_can_like_comment()
    {
        $this->actingAs($user = factory('App\User')->create());
        $post = factory('App\Post')->create();
        $comment = factory('App\Comment')->create(['user_id' => $user->id, 'post_id' => $post->id]);
        $like = factory('App\CommentLike')->create(['comment_id' => $comment->id, 'user_id' => $user->id]);
        $this->post('/comments/' . $comment->id . '/like', $like->toArray())->assertRedirect('/posts');
    }

    public function test_guest_can_not_like_post()
    {
        $post = factory('App\Post')->create();
        $comment = factory('App\Comment')->create(['post_id' => $post->id]);
        $like = factory('App\CommentLike')->create(['comment_id' => $comment->id]);
        $this->post('/comments/' . $comment->id . '/like', $like->toArray())->assertRedirect('/login');
    }

    public function test_dislike_comment_by_authenticated_user()
    {
        $this->actingAs($user = factory('App\User')->create());
        $post = factory('App\Post')->create();
        $comment = factory('App\Comment')->create(['user_id' => $user->id, 'post_id' => $post->id]);
        $like = factory('App\CommentLike')->create(['comment_id' => $comment->id, 'user_id' => $user->id]);
        $this->delete('/comments/'. $comment->id . '/dislike')->assertRedirect('/posts');
    }

    public function test_a_comment_like_belongs_to_a_user()
    {
        $like = factory('App\CommentLike')->create();
        $this->assertInstanceOf('App\User', $like->user);
    }

    public function test_like_belongs_to_a_comment()
    {
        $like = factory('App\CommentLike')->create();
        $this->assertInstanceOf('App\Comment', $like->comment);
    }
}
