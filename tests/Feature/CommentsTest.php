<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommentsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // public function testExample()
    // {
    //     $response = $this->get('/posts');

    //     $response->assertStatus(200);
    // }

    public function commentPublish($overrides = [])
    {  
        $this->actingAs(factory('App\User')->create());
        $post = factory('App\Post')->create();
        $comment = factory('App\Comment')->make(array_merge(['post_id' => $post->id], $overrides));
        return $this->post('/posts/'. $post->id . '/comment', $comment->toArray());
    }



    public function test_a_user_can_see_post_comments()
    {
        $post = factory('App\Comment')->create();
        $comment = factory('App\Comment')->create(['post_id' => $post->id]);

        $response = $this->get('/posts/'. $post->id );
        $response->assertSee($comment->body);

    }

    public function test_every_comment_belongs_to_a_user()
    {
        $post = factory('App\Comment')->create();
        $comment = factory('App\Comment')->create(['post_id' => $post->id]);

        $response = $this->get('/posts/'. $post->id );
        $this->assertInstanceOf(\App\User::class, $comment->user);
    }

    public function test_authenticated_user_can_send_comment()
    {
        $this->actingAs($user = factory('App\User')->create());
        $post = factory(\App\Post::class)->create();
        $comment = factory(\App\Comment::class)->create(['post_id' => $post->id, 'user_id' => $user->id]);

        $this->post('/posts/' . $post->id . '/comment', $comment->toArray());

        $this->get('/posts/'. $post->id)
            ->assertSee($comment->text);
    }

    public function test_comment_text_required()
    {
        $this->commentPublish(['text' => null])
            ->assertSessionHasErrors('text');
    }

    public function test_a_authenticated_user_can_see_reply_form ()
    {
        $this->actingAs($user = factory('App\User')->create());
        $comment = factory('App\Comment')->create();

        $this->get('/comments/'. $comment->id)
            ->assertStatus(200);
    }

    public function test_guest_can_not_see_reply_form ()
    {
        $comment = factory('App\Comment')->create();

        $this->get('/comments/'. $comment->id)
            ->assertRedirect('/login');
    }
}
