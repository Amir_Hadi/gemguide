<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FollowTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_an_authenticated_user_can_follow()
    {
        $this->actingAs($user = factory('App\User')->create());
        $following = factory('App\User')->create();
        $follow = factory('App\Follow')->create(['follower' => $user->id, 'following' => $following->id]);
        $this->post('/users/'. $user->id . '/follow', $follow->toArray())
            ->assertRedirect('/users/'. $user->id);
    }


    public function follow()
    {

    }

}
