<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostLikeTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // public function testExample()
    // {
    //     $response = $this->get('/login');

    //     $response->assertStatus(200);
    // }

    public function test_authenticated_user_can_like_posts()
    {
        $this->actingAs($user = factory('App\User')->create());
        $post = factory('App\Post')->create();
        $postlike = factory('App\PostLike')->make(['user_id' => $user->id, 'post_id' => $post->id]);
        $this->post('/posts/'. $post->id . '/like', $postlike->toArray());
        $this->get('/posts/'. $post->id)->assertStatus(200);

    }

    public function test_guest_can_not_like_posts ()
    {
        $post = factory('App\Post')->create();
        $like = factory('App\PostLike')->create(['post_id' => $post->id]);
        $this->post('/post/' . $post->id . '/like', $like->toArray())->assertRedirect('/login');
    }

    // public function test_authenticated_user_can_dislike_posts() 
    // {
        
    // }


    public function like()
    {

    }
}
