<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostTest extends TestCase
{
    use RefreshDatabase;

    // function test_posts()
    // {
    //     $response = $this->get('/posts');
    //     $response->assertStatus(200);
    // }

    function test_a_user_can_see_a_post()
    {
        $this->actingAs(factory('App\User')->create());
        $post = factory('App\Post')->create();
        $response = $this->get('/posts/'.$post->id);
        $response->assertSee($post->title);
        $response->assertSee($post->text);
    }

    public function test_guest_can_not_see_post()
    {
        $post = factory('App\Post')->create();
        $this->get('/posts/'. $post->id)->assertRedirect('/login');
    }

    function test_an_authenticated_user_can_create_post()
    {
        $this->actingAs(factory(\App\User::class)->create());
        $post = factory(\App\Post::class)->make();
        $this->post('/posts/create', $post->toArray());
        $this->get('/posts/'. $post->id)
            ->assertSee($post->title)
            ->assertSee($post->text);
    }

    function test_an_unauthenticated_user_cant_create_post()
    {
        $post = factory(\App\Post::class)->make();
        $this->post('/posts/create', $post->toArray())->assertRedirect('/login');
    }

    public function test_a_user_can_see_comments_associated_with_post()
    {
        $this->actingAs(factory('App\User')->create());
        $post = factory('App\Post')->create();
        $comment = factory('App\Comment')->create(['post_id'=> $post->id]);

        $response = $this->get('/posts/'.$post->id);
        $response->assertSee($comment->text);
    }

    public function test_post_title_is_required()
    {
        $this->publishPost(['title' => null])
            ->assertSessionHasErrors('title');
    }

    public function test_text_is_required()
    {
        $this->publishPost(['text' => null])
            ->assertSessionHasErrors('text');
    }

    public function test_view_specific_user_posts()
    {
        $this->actingAs($user = factory('App\User')->create());
        $postByUser = factory('App\Post')->create(['user_id' => $user->id]);
        $postNotByUser = factory('App\Post')->create();
        $this->get('/users/'. $user->id)
            ->assertSee($postByUser->title)
            ->assertDontSee($postNotByUser->title);
    }

    public function publishPost($overrides = [])
    {
        $this->actingAs(factory('App\User')->create());
        $post = factory('App\Post')->make($overrides);
        return $this->post('/posts/create', $post->toArray());
    }

    
    
}
