<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->get('/')
            ->assertRedirect('/login');
    }

    public function test_authenticated_user_can_not_see_login_form()
    {
        $this->actingAs($user = factory('App\User')->create());
        $this->get('/login')
            ->assertRedirect('home');
    }

    public function test_authenticated_user_can_not_register_form()
    {
        $this->actingAs($user = factory('App\User')->create());
        $this->get('/register')->assertRedirect('home');
    }

    public function test_register_name_validation()
    {
        $this->signUp(['name' => null])
            ->assertSessionHasErrors('name');
    }

    public function test_register_email_validation()
    {
        $this->signUp(['email' => null])
            ->assertSessionHasErrors('email');
    }

    public function test_register_password_validation() 
    {
        $this->signUp(['password' => null])
            ->assertSessionHasErrors('password');
    }

    public function test_login_username_validation()
    {
        $this->login(['email' => null])
            ->assertSessionHasErrors('email');
    }

    public function test_login_password_validation()
    {
        $this->login(['password' => null])
            ->assertSessionHasErrors('password');
    }

    public function login($overrides = [])
    {
        $user = factory('App\User')->make($overrides);
        return $this->post('/login', $user->toArray());
    }

    public function signUp($overrides = [])
    {
        $user = factory('App\User')->make($overrides);
        return $this->post('/register', $user->toArray());
    }

}
