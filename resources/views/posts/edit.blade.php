@extends('layouts.main')

@section('content')
    <div class="fm">

      <ul class="ca bow box afo">

        <li class="rv b agz">
          <form action="/posts/{{$post->id}}/update" method="POST">
            @csrf
            @method('PATCH')
            <div class="input-group">
              <input type="text" class="form-control" name="title" id="title" value="{{$post->title}}">
            </div>
            <div class="input-group">
              <textarea class="form-control" name="text" id="text" cols="30" rows="10">
                {{$post->text}}
              </textarea>
            </div>
            <div class="input-group">
              <button type="submit" class="cg ns yf">
                edit post
              </button>
            </div>
          </form>
        </li>
      </ul>
    </div>
@endsection