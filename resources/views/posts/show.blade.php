@extends('layouts.main')

@section('content')
  <div class="fm">

    <ul class="ca bow box afo">
          <li class="rv b agz">
            <img class="bos vb yb aff" src="/postimage/{{$post->id}}">
            <div class="rw">
              <div class="bpb">
                <small class="acx axc">{{$post->created_at->diffForHumans()}}</small>
                <h3>{{$post->title}}</h3>
                <h6>{{$post->user->name}}</h6>
              </div>

              <p>
               {{$post->text, 0, 250}}
              </p>
              <div class="row">
                  <p>
                    @if($post->postlikes()->where('user_id', auth()->id())->get()->count()>0)
                      <form  action="/post/{{$post->id}}/dislike" method="POST" >
                        @csrf
                        <button type="submit" style = "background: red; color:white;" class="form-control" name="like" id="like" value="like">like
                        <span>{{$post->postlikes->count()}}</span>
                      </form>
                    @else
                      <form  action="/post/{{$post->id}}/like" method="POST" >
                        @csrf
                        <button type="submit" class="form-control" name="like" id="like" value="like">like
                        <span>{{$post->postlikes->count()}}</span>
                      </form>
                    @endif
                  </p>

              </div>
              <div class="row">
                @if(auth()->check() && $post->user_id == auth()->id())
                  <form  action="/post/{{$post->id}}/delete" method="POST" >
                    @csrf
                    <button type="submit" style = "background: orange; color:white;" class="form-control" name="like" id="like" value="like">delete
                  </form>
                @endif

              </div>
              @if(auth()->id() == $post->user_id)
                <div class="row">
                  <a href="/posts/{{$post->id}}/edit" class="form-control" style="background: blue; color:white; text-align: center;">edit</a>
                </div>
              @endif

              <form action="/posts/{{$post->id}}/comment" method="POST">
                @csrf
                <div class="input-group">
                  <textarea class="form-control" name="text" id="text" cols="30" rows="10">
                  </textarea>
                </div>
                <div class="input-group">
                  <button type="submit" class="cg ns yf">
                    add comment
                  </button>
                </div>
              </form>

              <ul class="bow afa">
                @if($post->comments->count()>0)
                  @foreach($post->comments()->take(5)->get() as $comment)
                    <li class="rv afh">
                      <img class="bos vb yb aff" src="assets/img/avatar-fat.jpg">
                      <div class="rw" data-id={{$comment->id}}}}>
                        <strong> {{$comment->user->name}} </strong>
                        {{$comment->text}}
                        <br>
                        @if(auth()->check())
                          <a href="/comments/{{$comment->id}}">reply to {{$comment->user->name}}</a>
                          <br>
                        <a href="/comments/{{$comment->id}}/like" style="color:red">like comment  {{$comment->likes->count()}}</a>
                        @endif
                      </div>
                    </li>
                  @endforeach
                @endif
                {{-- <li class="rv">
                  <img
                    class="bos vb yb aff"
                    src="assets/img/avatar-mdo.png">
                  <div class="rw">
                    <strong>Mark Otto: </strong>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                  </div>
                </li> --}}
              </ul>
            </div>
          </li>

      {{-- <li class="rv b agz">
        <img
          class="bos vb yb aff"
          src="assets/img/avatar-fat.jpg">
        <div class="rw">
          <div class="bpd">
            <div class="bpb">
              <small class="acx axc">12 min</small>
              <h6>Jacob Thornton</h6>
            </div>
            <p>
              Donec id elit non mi porta gravida at eget metus. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            </p>
          </div>
        </div>
      </li>

      <li class="rv b agz">
        <img
          class="bos vb yb aff"
          src="assets/img/avatar-mdo.png">
        <div class="rw">
          <div class="bpb">
            <small class="acx axc">34 min</small>
            <h6>Mark Otto</h6>
          </div>

          <p>
            Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.
          </p>

          <div class="boy" data-grid="images">
            <img style="display: none" data-width="640" data-height="640" data-action="zoom" src="assets/img/instagram_3.jpg">
          </div>

          <ul class="bow">
            <li class="rv">
              <img
                class="bos vb yb aff"
                src="assets/img/avatar-dhg.png">
              <div class="rw">
                <strong>Dave Gamache: </strong>
                Donec id elit non mi porta gravida at eget metus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Donec ullamcorper nulla non metus auctor fringilla. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Sed posuere consectetur est at lobortis.
              </div>
            </li>
          </ul>
        </div>
      </li> --}}
    </ul>
  </div>
  <div class="fj">
    <div class="alert ro alert-dismissible d-none vy" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <a class="rj" href="profile/index.html">Visit your profile!</a> Check your self, you aren't looking well.
    </div>
    @if($errors->count()>0)
      <div class="alert ro alert-dismissible d-none vy" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        @foreach($errors->all() as $error)
          <a class="rj" href="profile/index.html">{{$error}}</a><br>
        @endforeach
      </div>
    @endif

    <div class="pz afo d-none vy">
      <div class="qa">
        <h6 class="afh">Sponsored</h6>
        <div data-grid="images" data-target-height="150">
          <img class="bos" data-width="640" data-height="640" data-action="zoom" src="assets/img/instagram_2.jpg">
        </div>
        <p><strong>It might be time to visit Iceland.</strong> Iceland is so chill, and everything looks cool here. Also, we heard the people are pretty nice. What are you waiting for?</p>
        <button class="cg nz ok">Buy a ticket</button>
      </div>
    </div>

    <div class="pz afo d-none vy">
      <div class="qa">
      <h6 class="afh">Likes <small>· <a href="#">View All</a></small></h6>
      <ul class="bow box">
        <li class="rv afa">
          <img
            class="bos vb yb aff"
            src="assets/img/avatar-fat.jpg">
          <div class="rw">
            <strong>Jacob Thornton</strong> @fat
            <div class="bpa">
              <button class="cg nz ok">
                <span class="h ayi"></span> Follow</button>
            </div>
          </div>
        </li>
         <li class="rv">
          <a class="bpu" href="#">
            <img
              class="bos vb yb aff"
              src="assets/img/avatar-mdo.png">
          </a>
          <div class="rw">
            <strong>Mark Otto</strong> @mdo
            <div class="bpa">
              <button class="cg nz ok">
                <span class="h ayi"></span> Follow</button></button>
            </div>
          </div>
        </li>
      </ul>
      </div>
      <div class="qg">
        Dave really likes these nerds, no one knows why though.
      </div>
    </div>

    <div class="pz bpm">
      <div class="qa">
        © 2018 Bootstrap
        <a href="#">About</a>
        <a href="#">Help</a>
        <a href="#">Terms</a>
        <a href="#">Privacy</a>
        <a href="#">Cookies</a>
        <a href="#">Ads </a>
        <a href="#">Info</a>
        <a href="#">Brand</a>
        <a href="#">Blog</a>
        <a href="#">Status</a>
        <a href="#">Apps</a>
        <a href="#">Jobs</a>
        <a href="#">Advertise</a>
      </div>
    </div>
  </div>

@endsection