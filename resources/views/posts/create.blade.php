@extends('layouts.main')

@section('content')
    <div class="fm">

      <ul class="ca bow box afo">

        <li class="rv b agz">
          <form action="/posts/store" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="input-group">
              <input type="text" class="form-control" name="title" id="title">
            </div>
            <div class="input-group">
              <input type="file" name="image" id="image" value="image">
            </div>
            <div class="input-group">
              <input type="radio" value="true" name="private"> private <br>
            </div>
            {{-- <style>
              .listtype {
                display: none;
              }
            </style> --}}
            <div class="input-group listtype">
              <label for="listType">choose who would see or not?</label> <br>
              <hr>
              <input type="radio" id="listType" name="listType" value="none">none<br>
              <hr>
              <input type="radio" id="listType" name="listType" value="blacklist">blacklist<br>
              <hr>
              <input type="radio" id="listtype" name="listtype" value="whitelist">whitelist<br>
              <hr>
            </div>
            <div class="input-group">
              <textarea class="form-control" name="text" id="text" cols="30" rows="10">

              </textarea>
            </div>
            <div class="input-group">
              <input id="createpost" type="submit" class="cg ns yf" value="create post">
            </div>
          </form>
        </li>
      </ul>
    </div>
@endsection