@if(count($users) > 0 && auth()->check())
	@foreach($users as $user)
		<li class="b">
		  <div class="rv ady">
		    <img class="bos vb yb aff" src="assets/img/avatar-fat.jpg">
		    <div class="rw">
		      <form action="/users/{{$user->follower}}/follow" class="cg ns ok acx" method="POST">
		        <input type="submit" class="btn btn-primary" value="follow" name="follow" id="follow">
		      </form>
		      <strong> {{$user->name}} </strong>
		      <p>@fat - San Francisco</p>
		    </div>
		  </div>
		</li>
	@endforeach
@endif