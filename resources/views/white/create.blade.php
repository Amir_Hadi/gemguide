@extends('layouts.main')

@section('content')
    <div class="fm">

      <ul class="ca bow box afo">

        <li class="rv b agz">
          <form action="/whitelist/{{$post->id}}/store" method="POST">
            @csrf
            {{-- <style>
              .listtype {
                display: none;
              }
            </style> --}}
            <div class="input-group listtype">
              <label for="listType">choose who would see or not?</label> <br>
              <hr>
              @foreach(auth()->user()->friends() as $friend) 
                @php $person = \App\User::find($friend) @endphp
                <input type="checkbox" id="user_id[{{$person->id}}]" name="user_id[{{$person->id}}]" value="{{$person->id}}">{{$person->name}}<br>
                <hr>
              @endforeach
            </div>

            <div class="input-group">
              <input id="createlist" type="submit" class="cg ns yf" value="create list">
            </div>
          </form>
        </li>
      </ul>
    </div>
@endsection