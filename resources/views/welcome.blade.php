@extends('layouts.main')

@section('content')
    <div class="fm">

      <ul class="ca bow box afo">

        @if($posts->count() > 0)
          @foreach($posts as $post)
            @can('view', $post)
              <li class="rv b agz">
                <div class="rw">
                  <a href="/posts/{{$post->id}}"><img class="bos vb yb aff" src="/postimage/{{$post->id}}">

                    <div class="bpb">
                      <small class="acx axc">{{$post->created_at->diffForHumans()}}</small>
                      <h3>{{$post->title}}</h3>
                      <h6>{{$post->user->name}}</h6>
                    </div>

                    <p>
                     {{$post->text}}
                    </p>
                  </a>
                  <div class="row">
                    <p>
                      @if($post->postlikes()->where('user_id', auth()->id())->get()->count()>0)
                        <form  action="/post/{{$post->id}}/dislike" method="POST" >
                          @csrf
                          <button type="submit" style = "background: red; color:white;" class="form-control" name="like" id="like" value="like">like
                          <span>{{$post->postlikes->count()}}</span>
                        </form>
                      @else
                        <form  action="/post/{{$post->id}}/like" method="POST" >
                          @csrf
                          <button type="submit" class="form-control" name="like" id="like" value="like">like
                          <span>{{$post->postlikes->count()}}</span>
                        </form>
                      @endif
                    </p>

                </div>

                <div class="row">
                  @if(auth()->check() && $post->user_id == auth()->id())
                    <form  action="/post/{{$post->id}}/delete" method="POST">
                      @csrf
                      <button type="submit" style ="background: orange; color:white;" class="form-control" name="like" id="like" value="like">delete
                    </form>
                  @endif
                </div>

                @if(auth()->id() == $post->user_id)
                  <div class="row">
                    <a href="/posts/{{$post->id}}/edit" class="form-control"f style="background: blue; color:white; text-align: center;">edit</a>
                  </div>
                @endif
                <ul class="bow afa">
                  @if($post->comments->count()>0)
                    @foreach($post->comments()->where('comment_id', 0)->take(5)->get() as $comment)
                      <li class="rv afh">
                        <img class="bos vb yb aff" src="assets/img/avatar-fat.jpg">
                        <div class="rw" data-id={{$comment->id}}}}>
                          <strong> {{$comment->user->name}} </strong>
                           @if($comment->comment_id != 0)
                            <small> reply to {{$comment->comment->user->name}} </small>
                           @endif
                           {{$comment->text}}
                          <br>
                          <a href="/comments/{{$comment->id}}">reply {{$comment->user->name}}  </a>
                          <br>
                          @if(auth()->check() && $comment->likes()
                                                ->where('user_id', auth()->id())
                                                ->where('comment_id', $comment->id)->get()->count() > 0)
                            <form action="/comments/{{$comment->id}}/dislike" method="POST">
                              @csrf
                              {{method_field('DELETE')}}
                              <span>like comment  {{$comment->likes->count()}}</span>
                              <input type="submit" name="dislike" id="dislike" value="dislike" class="form-control">
                              
                            </form>
                          @else
                            
                            <form action="/comments/{{$comment->id}}/like" method="POST">
                              @csrf
                              <input type="submit" name="dislike" id="dislike" value="like comment  {{$comment->likes->count()}}" class="form-control">
                              
                            </form>

                          @endif
                        </div>
                      </li>
                    @endforeach
                  @endif
                  {{-- <li class="rv">
                    <img
                      class="bos vb yb aff"
                      src="assets/img/avatar-mdo.png">
                    <div class="rw">
                      <strong>Mark Otto: </strong>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                    </div>
                  </li> --}}
                </ul>
                <div class="commentloader" data-id="{{$post->id}}" style="cursor: pointer; padding: 10px; border: 1px solid;">
                  click me to load more
                </div>

                  {{-- <ul class="bow afa">
                    <li class="rv afh">
                      <img
                        class="bos vb yb aff"
                        src="assets/img/avatar-fat.jpg">
                      <div class="rw">
                        <strong>Jacon Thornton: </strong>
                        Donec id elit non mi porta gravida at eget metus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Donec ullamcorper nulla non metus auctor fringilla. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Sed posuere consectetur est at lobortis.
                      </div>
                    </li>
                    <li class="rv">
                      <img
                        class="bos vb yb aff"
                        src="assets/img/avatar-mdo.png">
                      <div class="rw">
                        <strong>Mark Otto: </strong>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                      </div>
                    </li>
                  </ul> --}}
                </div>
              
              </li>
            @endcan
          @endforeach
        @endif

        {{-- <li class="rv b agz">
          <img
            class="bos vb yb aff"
            src="assets/img/avatar-fat.jpg">
          <div class="rw">
            <div class="bpd">
              <div class="bpb">
                <small class="acx axc">12 min</small>
                <h6>Jacob Thornton</h6>
              </div>
              <p>
                Donec id elit non mi porta gravida at eget metus. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              </p>
            </div>
          </div>
        </li>

        <li class="rv b agz">
          <img
            class="bos vb yb aff"
            src="assets/img/avatar-mdo.png">
          <div class="rw">
            <div class="bpb">
              <small class="acx axc">34 min</small>
              <h6>Mark Otto</h6>
            </div>

            <p>
              Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.
            </p>

            <div class="boy" data-grid="images">
              <img style="display: none" data-width="640" data-height="640" data-action="zoom" src="assets/img/instagram_3.jpg">
            </div>

            <ul class="bow">
              <li class="rv">
                <img
                  class="bos vb yb aff"
                  src="assets/img/avatar-dhg.png">
                <div class="rw">
                  <strong>Dave Gamache: </strong>
                  Donec id elit non mi porta gravida at eget metus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Donec ullamcorper nulla non metus auctor fringilla. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Sed posuere consectetur est at lobortis.
                </div>
              </li>
            </ul>
          </div>
        </li> --}}
      </ul>
    </div>

@endsection