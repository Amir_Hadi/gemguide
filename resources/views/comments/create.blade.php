@extends('layouts.main')

@section('content')
	<div class="fm">

	  <ul class="ca bow box afo">

	    <li class="rv b agz">
	      <form action="/comments/{{$comment->id}}/reply" method="POST">
	        @csrf
	        <div class="input-group">
	          <textarea class="form-control" name="text" id="text" cols="30" rows="10">
	            
	          </textarea>
	        </div>
	        <div class="input-group">
	          <button type="submit" class="cg ns yf">
	            reply
	          </button>
	        </div>
	      </form>
	    </li>
	  </ul>
	</div>
	<div class="fj">
		@if($errors->count()>0)
		  <div class="alert ro alert-dismissible d-none vy" role="alert">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		    @foreach($errors->all() as $error)
		      <a class="rj" href="profile/index.html">{{$error}}</a><br>
		    @endforeach
		  </div>
		@endif
	</div>
@endsection