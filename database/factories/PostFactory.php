<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(App\User::class, function (Faker $faker) {
	static $password;
	return [
		'name' => $faker->name,
		'email' => $faker->unique()->safeEmail,
		'password' => $password ?: $password = bcrypt('secret'),
		'remember_token' => str_random(10),
		'api_token' => Str::random(60),
	];
});


$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'user_id' => function (){
        	return factory(App\User::class)->create()->id;
        },
        'private' => $faker->boolean,
        'image' => $faker->url,
        'title' => $faker->sentence,
        'mime' => $faker->word,
        'listType' => 'none',
        'text' => $faker->paragraph
    ];
});

$factory->define(App\Comment::class, function (Faker $faker) {
	return [
		'post_id' => function () {
			return factory(App\Post::class)->create()->id;
		},
		'user_id' => function (){
        	return factory(App\User::class)->create()->id;
        },
        'text' => $faker->paragraph
	];
});

$factory->define(App\PostLike::class, function (Faker $faker) {
	return [
		'user_id' => function() {
			return factory('App\User')->create()->id;
		},
		'post_id' => function() {
			return factory('App\Post')->create()->id;
		},
	];
});

$factory->define('App\CommentLike', function (Faker $faker){
	return [
		'comment_id' => function(){
			return factory('App\Comment')->create()->id;
		},
		'user_id' => function() {
			return factory('App\User')->create()->id;
		},
	];
});


$factory->define('App\Follow', function (Faker $faker){
	return [
		'follower' => function () {
			return factory('App\User')->create()->id;
		},
		'following' => function () {
			return factory('App\User')->create()->id;
		},
		'twoway' => $faker->boolean,
	];
});

$factory->define('App\Image', function (Faker $faker) {
	return [
		'user_id' => function() {
			factory('App\User')->crerate()->id;
		},
		'url' => $faker->string, 
	];
});