<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [ 'required', 'max:80'],
            'text' => ['required'],
            'image' => ['required', 'mimes:jpg,png,jpeg', 'max:1000'],
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'title must be initialized',
            'title.max' => 'maximum chars must be 80',
            'text.required' => 'text is required',
            'image.required' => 'you must upload an image'
        ];
    }
}
