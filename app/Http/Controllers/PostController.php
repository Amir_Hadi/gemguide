<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Post;
use http\Header;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Storage;
use App\Traits\ResponseTrait;


class PostController extends Controller
{
    use ResponseTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        if(!$this->checkHeaderContentType()){
            return $this->middleware('auth');
        }
    }

    public function apiIndex(){
        $posts = Post::all();
        if($this->checkHeaderContentType()) 
            return response()->json($posts);
        return view('posts.index', compact('posts'));
    }

    public function index()
    {
        $posts = Post::paginate(10);
        if(request()->header('Accept') == 'application/json'){
            return response()->json($posts);
        }
        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        if($this->checkHeaderContentType())
            return response()->json(['message' => 'page not found'], 404);
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        // dd(request('image'));

        $url = str_replace("/public", "", '/storage/'.Storage::putFile('/public/posts', request('image')));

        $post = Post::create([
            'user_id' => auth()->id(),
            'title' => $request->get('title'),
            'text' => $request->get('text'),
            'image' => $url,
            'listType' => $request->get('listType'),
            'private' => (boolean)request('private'),
            'mime' => request('image')->getMimeType(),
        ]);

        


        if(request()->header('Accept') == 'application/json'){
            return Helper::response($post, 200);
        }

        return $post->whereToGo();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $this->authorize('view', $post);
        if(request()->header('Accept') == 'application/json'){
            return Helper::response(['post' => $post, 'comments' => $post->comments]);
        }
        return view('posts.show', compact('post'));
    }

    public function showImage(Post $post)
    {
        $this->authorize('view', $post);
        header('Content-Type: '. $post->mime);
        if(file_exists(".".$post->image)){
            echo file_get_contents(".{$post->image}");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $this->authorize('update', $post);
        if(request()->header('Accept') == 'application/json'){
            return Helper::response($post, 200);
        }
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {
        $this->authorize('update', $post);
        if(auth()->check() && $post->user_id == auth()->id()){
            $post->title = $request->get('title');
            $post->text = $request->get('text');
            $post->save();
        }
        if(request()->header("Accept") == 'application/json'){
            return Helper::response($post, 200);
        }
        return redirect("/posts/{$post->id}");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $this->authorize('delete', $post);
        if($post->user_id === auth()->id()){
            $post->deletelikes();
            $post->deletecomments();
            $post->delete();
        }
        if(request()->header("Accept") == 'application/json'){
            return Helper::response('deleted successfully', 200);
        }
        return back();
    }

}
