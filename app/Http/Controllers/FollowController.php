<?php

namespace App\Http\Controllers;

use App\Follow;
use App\Helpers\Helper;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FollowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $user)
    {
//        dd($user);
        $follow = Follow::where('follower', $user->id)->where('following', auth()->id())->get()->first();
        if (auth()->check()) {
            if (empty($follow)) {

                Follow::create([
                    'follower' => auth()->id(),
                    'following' => $user->id
                ]);

            } elseif (empty($follow) == false &&
                $follow->twoway == false &&
                $follow->following == auth()->id()
            ) {

                DB::table('follows')->where('following', auth()->id())
                                    ->where('follower', $user->id)
                                    ->update(['twoway' => true]);

            }
        }

        
        if(request()->header('Accept') == 'application/json'){
            return Helper::response("now you are following {$user->name}", 200);
        }
        return redirect('/users/'. $user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Follow  $follow
     * @return \Illuminate\Http\Response
     */
    public function show(Follow $follow)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Follow  $follow
     * @return \Illuminate\Http\Response
     */
    public function edit(Follow $follow)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Follow  $follow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Follow $follow)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Follow  $follow
     * @return \Illuminate\Http\Response
     */
    public function destroy(Follow $follow)
    {
        //
    }
}
