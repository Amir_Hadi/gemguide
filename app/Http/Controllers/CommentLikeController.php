<?php

namespace App\Http\Controllers;

use App\CommentLike;
use Illuminate\Http\Request;
use App\Comment;

class CommentLikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Comment $comment)
    {
        if(auth()->check() && 1 > count(CommentLike::where('comment_id', $comment->id)->where('user_id', auth()->id())->get())){
            CommentLike::create([
                'comment_id' => $comment->id,
                'user_id' => auth()->id()
            ]);
        }
        return redirect('/posts');
    }

    public function dislike(Comment $comment)
    {
        $like = CommentLike::where('comment_id', $comment->id)->where('user_id', auth()->id())->get()->first();
        if(auth()->check() && $comment->id == $like->comment_id){
            $like->delete();
        }
        
        return redirect('/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CommentLike  $commentLike
     * @return \Illuminate\Http\Response
     */
    public function show(CommentLike $commentLike)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CommentLike  $commentLike
     * @return \Illuminate\Http\Response
     */
    public function edit(CommentLike $commentLike)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CommentLike  $commentLike
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CommentLike $commentLike)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CommentLike  $commentLike
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommentLike $commentLike)
    {
        //
    }
}
