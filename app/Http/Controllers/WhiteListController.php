<?php

namespace App\Http\Controllers;

use App\WhiteList;
use Illuminate\Http\Request;

class WhiteListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('whitelist.create', compact('post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach(request('user_id') as $user ){
            if(\App\User::find($user)) {
                BlackList::create([
                    'post_id' => $post->id,
                    'user_id' => $user
                ]);
            }
        }
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WhiteList  $whiteList
     * @return \Illuminate\Http\Response
     */
    public function show(WhiteList $whiteList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WhiteList  $whiteList
     * @return \Illuminate\Http\Response
     */
    public function edit(WhiteList $whiteList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WhiteList  $whiteList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WhiteList $whiteList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WhiteList  $whiteList
     * @return \Illuminate\Http\Response
     */
    public function destroy(WhiteList $whiteList)
    {
        //
    }
}
