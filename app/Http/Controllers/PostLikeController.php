<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\PostLike;
use Illuminate\Http\Request;
use App\Post;
use App\User;

class PostLikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function __construct()
//    {
////        return $this->middleware('auth');
//    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Post $post)
    {
        PostLike::create([
            'post_id' => $post->id,
            'user_id' => auth()->id()
        ]);

        if(request()->header('Accept') == 'application/json'){
            return Helper::response(['message' => " the post \"$post->title \"liked by ". auth()->user()->name]);
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PostLike  $postLike
     * @return \Illuminate\Http\Response
     */
    public function show(PostLike $postLike)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PostLike  $postLike
     * @return \Illuminate\Http\Response
     */
    public function edit(PostLike $postLike)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PostLike  $postLike
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PostLike $postLike)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PostLike  $postLike
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if(auth()->check() && PostLike::where('post_id', $post->id)
            ->where('user_id', auth()->id())->get()->first()){

            PostLike::where('post_id', $post->id)
            ->where('user_id', auth()->id())->get()->first()->delete();

        }
        return back();
    }
}
