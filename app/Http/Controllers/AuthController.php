<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function store(Request $request)
    {
    	$this->validate(request(), [
    		'email' => ['email', 'required', 'string'],
    		'password' => ['required', 'string']
    	]);

    	auth()->attempt($request->only(['email', 'password']));

    	if (!auth()->check()) {
    	    return 'false';
        }

    	$user = User::where('email', request('email'))->get()->first();

    	$user->api_token = Str::random(64);
    	$user->save();

		return response()->json(['message' => "welcome {$user->name}", "token" => $user->api_token ]);
    }

    public function destroy()
    {
        $authUser = auth()->user();

        $authUser->update([
            'api_token' => null,
        ]);

        return Helper::response("Logout successfully");
    }
}

