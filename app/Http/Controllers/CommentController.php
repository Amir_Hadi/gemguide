<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Helpers\Helper;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Requests\CommentRequest;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        if(request()->header('Accept') != 'application/json'){
            return $this->middleware('auth');
        }
    }

    public function index(Post $post)
    {
        return response()->json(['post' => $post, 'comments'=> $post->comments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request, \App\Post $post)
    {
        Comment::create([
            'post_id' => $post->id,
            'user_id' => auth()->id(),
            'comment_id' => 0,
            'text' => $request->get('text')
        ]);
        if($request->header('Accept') == 'application/json'){
            return Helper::response(['message' => 'your comment sent']);
        }
        return redirect("/posts/{$post->id}");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        return response()->json([$comment, $comment->post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }


    public function reply(Comment $comment)
    {
        return view('comments.create', compact('comment'));
    }

    public function replystore(CommentRequest $request, Comment $comment)
    {
        Comment::create([
            'user_id' => auth()->id(),
            'post_id' => $comment->post_id,
            'comment_id' => $comment->id,
            'text' => $request->get('text')
        ]);
        return redirect("/posts/{$comment->post_id}");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
