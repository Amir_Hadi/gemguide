<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function store(Request $request)
    {
        $this->validate(request(), [
            'name' => ['required', 'string'],
            'email' => ['email', 'required', 'string'],
            'password' => ['required', 'string', 'confirmed']
        ]);

        $user = new User([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password'))
        ]);

        $token = $user->createToken('remember token for application');

        $user->remember_token = $token->accessToken;

        $user->save();

        return response()->json([
            'message' => "welcome {$user->name}",
            'token' => $token->accessToken
        ]);
     }
    public function show(User $user){
    	if(request()->header('Content-Type') == 'application/json')
    		return response()->json($user);
    	return view('users.show', compact('user'));
    }

}
