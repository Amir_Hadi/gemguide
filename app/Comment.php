<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['text', 'user_id', 'post_id', 'comment_id'];

    public function user()
    {
    	return $this->belongsTo(\App\User::class, 'user_id');
    }

    public function post()
    {
    	return $this->belongsTo(\App\Post::class, 'post_id');
    }

    public function comments()
    {
    	return $this->hasMany(\App\Comment::class, 'comment_id');
    }

    public function comment()
    {
    	return $this->belongsTo(\App\Comment::class);
    }

    public function likes()
    {
        return $this->hasMany(\App\CommentLike::class, 'comment_id');
    }
}
