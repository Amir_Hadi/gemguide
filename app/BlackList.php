<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlackList extends Model
{

	protected $fillable = ['user_id', 'post_id'];

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function badguy()
    {
    	return $this->belongsTo('App\User', 'badguy');
    }


}
