<?php

namespace App\Helpers;

class Helper
{
    public static function response($data, $code = 200)
    {
        return response()->json([
            'status' => $code,
            'data' => $data,
        ]);
    }

    public static function responseByRequest($data, $string, $view=null){
        if(request()->header('Contet-Type') == 'application.json'){
            return self::response($data, 200);
        } else {
            return view($view, compact($string));
        }
    }
}
