<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function posts()
    {
        return $this->hasMany(\App\Post::class, 'user_id');
    }

    public function comments()
    {
        return $this->hasMany(\App\Comment::class, 'user_id');
    }

    public function likesposts()
    {
        return $this->hasMany(\App\PostLike::class, 'user_id');
    }

    public function likecomments()
    {
        return $this->hasMany(\App\CommentLike::class, 'user_id');
    }

    public function followers()
    {
        return $this->hasMany(\App\Follow::class, 'following');
    }

    public function followings()
    {
        return $this->hasMany(\App\Follow::class, 'follower');
    }

    public function friends()
    {
        $followers = \App\Follow::where('following', auth()->id())->pluck('follower')->toArray();
        $followings = \App\Follow::where('follower', auth()->id())->pluck('following')->toArray();
        $friends = array_merge($followers, $followings);
        return $friends;
    }

    public function suggestions(){
        $followers = \App\Follow::where('following', auth()->id())->where('twoway', 0)->get();
        return $followers;
    }

    public function blacklist()
    {
        return $this->hasMany('App\BlackList', 'user_id');
    }

    // public function likespost(Post $post)
    // {
    //     return $this->likes()->where('post_id', $post->id)->get()->post_id != null;
    // }
}
