<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    protected $fillable = ['follower', 'following', 'twoway'];

    public function following(){
    	return $this->belongsTo(\App\User::class, 'following', 'id');
    }

    public function follower(){
    	return $this->belongsTo(\App\User::class, 'follower', 'id');
    }
}
