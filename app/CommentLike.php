<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentLike extends Model
{
    protected $fillable = ['comment_id', 'user_id'];

    public function comment()
    {
    	return $this->belongsTo(\App\Comment::class, 'comment_id');
    }

    public function user()
    {
    	return $this->belongsTo(\App\User::class, 'user_id');
    }

}
