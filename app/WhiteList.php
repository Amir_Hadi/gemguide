<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhiteList extends Model
{
    protected $guarded = [];
}
