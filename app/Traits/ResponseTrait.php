<?php

namespace App\Traits;

use Illuminate\Http\Request;

trait ResponseTrait {
	

	public function checkHeaderContentType()
	{
		return request()->header('Content-Type') == 'application/json';
	}

	// public function  

}