<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Post;

class Post extends Model
{
    protected $fillable = ['title', 'text', 'user_id', 'private', 'image', 'mime', 'listType'];

    public function user()
    {
    	return $this->belongsTo(\App\User::class, 'user_id');
    }

    public function comments()
    {
    	return $this->hasMany(\App\Comment::class, 'post_id');
    }

    public function postlikes()
    {
    	return $this->hasMany(\App\PostLike::class, 'post_id');
    }


    public function deletecomments()
    {
        DB::table('comments')->where('post_id', $this->id)->delete();
    }

    public function deletelikes()
    {
        DB::table('post_likes')->where('post_id', $this->id)->delete();
    }

    public function whereToGo()
    {
        if($this->private){
            if ($this->listType == 'none') {
                return redirect("/");
            } elseif ( $this->listType == 'blacklist') {
                return redirect("/blacklist/{$this->id}/create");
            } else {
                return redirect("/whitelist/{$this->id}/create");
            }
        } else {
            // if($this->listType == 'blacklist'){
            //     return redirect("/blacklist/{$this->id}/create");
            // } else {
                return redirect("/");
            // }
        }
    } 

    public function blacklist() 
    {
        return $this->hasMany(\App\BlackList::class, 'post_id');
    }

    public function whitelist() 
    {
        return $this->hasMany(\App\WhiteList::class, 'post_id');
    }

    public function postAuthorization(User $user, Post $post)
    {
        if($post->private){
            if ($post->listType == 'none') {
                if ($user->id == $post->user_id || in_array(auth()->id(), auth()->user()->friends())) {
                    return true;
                } else {
                    return false;
                }
            } elseif ($post->listType == 'blacklist') {
                return !in_array(auth()->id(), $post->blacklist()->pluck('id')->toArray());
            } elseif ($post->listType == 'whitelist') {
                return in_array(auth()->id(), $post->whitelist()->pluck('id')->toArray());
            }
        } else {
            return true;
        }
    }

}
