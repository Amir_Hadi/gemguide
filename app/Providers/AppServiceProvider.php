<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
// use Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.suggestions', function ($view) {
            $view->with('users', auth()->user()->suggestions());
        });
        view()->composer('layouts.main', function ($view) {
            $view->with('friends', \App\User::whereIn('id', auth()->user()->friends())->get());
        });
    }
}
